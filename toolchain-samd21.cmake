set(CMAKE_SYSTEM_NAME Generic)

set(ARDUINO_SDK_PATH "$ENV{ARDUINO_SDK_PATH}" CACHE STRING "Arduino SDK Path")
set(ARDUINO_SAMD_PATH "$ENV{ARDUINO_SAMD_PATH}" CACHE STRING "Arduino SAMD Path")
set(CMSIS_ATMEL_PATH "$ENV{CMSIS_ATMEL_PATH}" CACHE STRING "CMSIS-Atmel Path")
set(CMSIS_PATH "$ENV{CMSIS_PATH}" CACHE STRING "CMSIS Path")

set(CMAKE_C_COMPILER arm-none-eabi-gcc)
set(CMAKE_CXX_COMPILER arm-none-eabi-g++)

set(ARDUINO_C_FLAGS
    -mcpu=cortex-m0plus
    -mthumb
    -g
    -Os
    -w
    -std=gnu11
    -ffunction-sections
    -fdata-sections
    --param max-inline-insns-single=500
    -MMD
    -DF_CPU=48000000L
    -DARDUINO=10809
    -DARDUINO_SAMD_MKRZERO
    -DARDUINO_ARCH_SAMD
    -DUSE_ARDUINO_MKR_PIN_LAYOUT
    -D__SAMD21G18A__
    -DUSB_VID=0x2341
    -DUSB_PID=0x804f
    -DUSBCON
    \"-DUSB_MANUFACTURER=\\\"Arduino LLC\\\"\"
    \"-DUSB_PRODUCT=\\\"Arduino MKRZero\\\"\"
    -DARDUINO_SAMD_MKRVIDOR4000
    -larm_cortexM0l_math
    -lm
)
string(REPLACE ";" " " ARDUINO_C_FLAGS "${ARDUINO_C_FLAGS}")

set(ARDUINO_CXX_FLAGS
    -mcpu=cortex-m0plus
    -mthumb
    -g
    -Os
    -w
    -std=gnu++11
    -ffunction-sections
    -fdata-sections
    -fno-threadsafe-statics
    --param max-inline-insns-single=500
    -fno-rtti
    -fno-exceptions
    -MMD
    -DF_CPU=48000000L
    -DARDUINO=10809
    -DARDUINO_SAMD_MKRZERO
    -DARDUINO_ARCH_SAMD
    -DUSE_ARDUINO_MKR_PIN_LAYOUT
    -D__SAMD21G18A__
    -DUSB_VID=0x2341
    -DUSB_PID=0x804f
    -DUSBCON
    \"-DUSB_MANUFACTURER=\\\"Arduino LLC\\\"\"
    \"-DUSB_PRODUCT=\\\"Arduino MKRZero\\\"\"
    -DARDUINO_SAMD_MKRVIDOR4000
    -larm_cortexM0l_math
    -lm
)
string(REPLACE ";" " " ARDUINO_CXX_FLAGS "${ARDUINO_CXX_FLAGS}")

set(ARDUINO_LINK_FLAGS
    -Os
    -Wl,--gc-sections
    \"-T${ARDUINO_SAMD_PATH}/variants/mkrzero/linker_scripts/gcc/flash_with_bootloader.ld\"
    --specs=nano.specs
    --specs=nosys.specs
    -mcpu=cortex-m0plus
    -mthumb
    -Wl,--check-sections
    -Wl,--gc-sections
    -Wl,--unresolved-symbols=report-all
    -Wl,--warn-common
    -Wl,--warn-section-align
    \"-L${CMSIS_PATH}/CMSIS/Lib/GCC/\"
    -larm_cortexM0l_math
    -lm
)
string(REPLACE ";" " " ARDUINO_LINK_FLAGS "${ARDUINO_LINK_FLAGS}")

set(CMAKE_C_FLAGS "${ARDUINO_C_FLAGS}" CACHE STRING "C Compiler Base Flags")
set(CMAKE_CXX_FLAGS "${ARDUINO_CXX_FLAGS}" CACHE STRING "C++ Compiler Base Flags")
set(CMAKE_EXE_LINKER_FLAGS "${ARDUINO_LINK_FLAGS}" CACHE STRING "Exe Linker Flags")
