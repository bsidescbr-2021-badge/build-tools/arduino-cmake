# BSides 2021 MKRZero variant

This is a slight modification of the stock Arduino MKRZero variant
file, to enable external interrupts on the UART bridge receive pin
(PA12) so we can safely suspend and resume SAMD operations.
