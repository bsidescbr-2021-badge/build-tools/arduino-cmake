set(CMAKE_SYSTEM_NAME Generic)

set(ARDUINO_SDK_PATH "$ENV{ARDUINO_SDK_PATH}" CACHE STRING "Arduino SDK Path")

set(CMAKE_C_COMPILER avr-gcc)
set(CMAKE_CXX_COMPILER avr-g++)

set(ARDUINO_C_FLAGS
    -Wno-frame-address
    -ffunction-sections
    -fdata-sections
    -mmcu=atmega328p
    -DF_CPU=16000000
    -DARDUINO=100
    -std=gnu99
    -Os
)
string(REPLACE ";" " " ARDUINO_C_FLAGS "${ARDUINO_C_FLAGS}")

set(ARDUINO_CXX_FLAGS
    -Wno-frame-address
    -ffunction-sections
    -fdata-sections
    -fno-exceptions
    -mmcu=atmega328p
    -DF_CPU=16000000
    -DARDUINO=100
    -Os
)
string(REPLACE ";" " " ARDUINO_CXX_FLAGS "${ARDUINO_CXX_FLAGS}")

set(CMAKE_C_FLAGS "${ARDUINO_C_FLAGS}" CACHE STRING "C Compiler Base Flags")
set(CMAKE_CXX_FLAGS "${ARDUINO_CXX_FLAGS}" CACHE STRING "C++ Compiler Base Flags")
